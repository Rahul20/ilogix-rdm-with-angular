﻿
//Define an angular module for our app
var sampleApp = angular.module('driverApp', ['ngGrid']);
//this has been taken from a tutorial to understand angular routing
//Define Routing for app

sampleApp.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider.
        when('/AddNewOrder', {
            templateUrl: 'templates/add_order.html',
            controller: 'AddOrderController'
        }).
        when('/ShowOrders', {
            templateUrl: 'templates/show_orders.html',
            controller: 'ShowOrdersController'
        }).
             when('/Home', {
                 templateUrl: 'templates/home.html',
                 controller: 'HomeController'
             }).
            when('/DriverMessages', {
                templateUrl: 'templates/show_drivers.html',
                controller: 'currentDriversController'
            }).
          when('/DriverSql', {
              templateUrl: 'templates/show_driversql.html',
              controller: 'driversqlcontroller'
          }).
             when('/DriverDeactivation', {
                 templateUrl: 'templates/show_driverdeactivate.html',
                 controller: 'driverdeactivatecontroller'
             }).
              when('/DriverLogging', {
                  templateUrl: 'templates/show_driverlogging.html',
                  controller: 'driverloggingcontroller'
              }).
        otherwise({
            redirectTo: '/DriverMessages'
        });
  }]);


sampleApp.controller('AddOrderController', function ($scope) {

    $scope.message = 'This is Add new order screen';

});


sampleApp.controller('ShowDriversController', function ($scope) {

    $scope.message = 'This is View Drivers screen';

});

sampleApp.controller('ShowOrdersController', function ($scope, $http, customerService) {
    //Perform the initialization
    $scope.myData = '';
    // $scope.editableInPopup = '<button id="editBtn" type="button" class="btn btn-primary" ng-click="edit(row.entity)" >Send Message</button> '
    $scope.gridOptions = {
        data: 'myData',
        showFilter: true,
        showGroupPanel: true,
        enableColumnResize: true,
        columnDefs: [
        { field: 'DrvId', displayName: 'Id', width: "30", pinnable: true },
        { field: 'DriverNumber', displayName: 'Driver Number', width: "80", pinnable: true },

        { field: 'LastUpdated', displayName: 'LastUpdated', width: "160", pinnable: true },
        { field: 'MobileDeviceId', displayName: 'DeviceId', width: "100", pinnable: true },
        { field: 'MobileEmail', displayName: 'Email', width: "150", pinnable: true },
        { field: 'MobilePhoneNumber', displayName: 'Phone', width: "80", pinnable: true },
        { field: 'MobileCarrier', displayName: 'Carrier', width: "100", pinnable: true },
        { field: 'MobilePlatform', displayName: 'Platform', width: "60", pinnable: true },
        { field: 'MobilePlatformVersion', displayName: 'PlatformVersion', width: "50", pinnable: true },

        { field: 'MobileTestBuild', displayName: 'TestBuild', width: "50", pinnable: true },
        { field: 'CurrentlyActive', displayName: 'CurrentlyActive', width: "50", pinnable: true },

         { field: 'GoogleClientKey', displayName: 'GoogleClientKey', width: "200", pinnable: true },
            { field: 'StateId', displayName: 'StateId', width: "50", pinnable: true },
               { field: 'FleetId', displayName: 'FleetId', width: "50", pinnable: true }
       // {displayName:'Pop up',cellTemplate:$scope.editableInPopup}
        ],
        canSelectRows: false,
        displaySelectionCheckbox: true
    };


    function fetchData() {
        //  alert("Timeout");
        setTimeout(function () {
            $http({ method: 'GET', url: 'http://localhost:64827/api/currentdriver' }).
  success(function (data, status, headers, config) {
      // this callback will be called asynchronously
      // when the response is available
      //window.alert(data);
      $scope.myData = data;
      if (!$scope.$$phase) {
          $scope.$apply();
      }
  }).
  error(function (data, status, headers, config) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
  });

        }, 1000);
    }

    //window.alert("Alert2");

    fetchData();
});

sampleApp
    .factory('dataFactory', ['$http', function ($http) {

        var urlBase = '/api/currentdriver';
        var dataFactory = {};

        dataFactory.getCurrentDriver = function () {
            return $http.get(urlBase);
        };

        dataFactory.getCurrentDriver = function (id) {
            return $http.get(urlBase + '/' + id);
        };

        dataFactory.insertCurrentDriver = function (driver) {
            return $http.post(urlBase, driver);
        };

        dataFactory.updateCurrentDriver = function (driver) {
            return $http.put(urlBase + '/' + driver.drvId, driver)
        };

        dataFactory.deleteCurrentDriver = function (id) {
            return $http.delete(urlBase + '/' + id);
        };

        //dataFactory.getOrders = function (id) {
        //    return $http.get(urlBase + '/' + id + '/orders');
        //};

        return dataFactory;
    }]);


//sampleApp.controller('currentDriversController', ['$scope', 'dataFactory',
//        function ($scope, dataFactory) {

//            $scope.status;
//            $scope.drivers;
//            $scope.message = "Testing here";

//            getDrivers();

//            function getDrivers() {
//                dataFactory.getDrivers()
//                    .success(function (driver) {
//                        $scope.drivers = driver;
//                    })
//                    .error(function (error) {
//                        $scope.status = 'Unable to load customer data: ' + error.message;
//                    });
//            };
//        }]);


sampleApp.service('customerService', function ($http, $q) {
    this.getCustomers = function () {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: 'api/currentdriver'
        })
        .success(function (data, status, headers, config) {
            // any required additional processing here
            deferred.resolve(data);
            console.log("Received:" + data);
        })
        .error(function (data, status) {
            deferred.reject(data);
        });
        return deferred.promise;
    }
    this.sendMsg = function (msg, reg, sqlInject) {
        console.log("Sending Message to API");
        var deferred = $q.defer();
        var urlToConnect = "";
        if (sqlInject){
            urlToConnect = 'api/currentdriver?id=3&text=' + msg + '&regid=' + reg + '&sqlInject=true';
            }
        else
        {
            urlToConnect = 'api/currentdriver?id=3&text=' + msg + '&regid=' + reg + '&sqlInject=false';
        }

            $http({
                method: 'GET',
                //url: 'api/currentdriver?id=3&text=' + cust + '&regid=' + reg,
                url: urlToConnect,
                params: msg
            })
            .success(function (data, status, headers, config) {
                // any required additional processing here
                deferred.resolve(data);
                console.log("Received:" + data);
            })
            .error(function (data, status) {
                deferred.reject(data);
            });
        return deferred.promise;
    }
});
sampleApp.controller('currentDriversController', function ($scope, $http, customerService) {
    //Perform the initialization
    $scope.filterOptions = {
        filterText: ''
    };
    $scope.myData = '';
    $scope.editableInPopup = '<button id="editBtn" type="button" class="btn btn-primary" ng-click="edit(row.entity)" >Send</button> '
    $scope.gridOptions = {
        data: 'myData',
        showFilter: true,
        showGroupPanel: true,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        columnDefs: [
             {displayName:'Msg',cellTemplate:$scope.editableInPopup, width: "60", pinnable: true},
        { field: 'DrvId', displayName: 'Id', width: "30", pinnable: true },
        { field: 'DriverNumber', displayName: 'Number', width: "80", pinnable: true },
        
        { field: 'LastUpdated', displayName: 'LastUpdated', width: "160", pinnable: true },
        { field: 'MobileDeviceId', displayName: 'DeviceId', width: "100", pinnable: true },
        { field: 'MobileEmail', displayName: 'Email', width: "150", pinnable: true },
        { field: 'MobilePlatform', displayName: 'Type', width: "60", pinnable: true },
        { field: 'MobilePlatformVersion', displayName: 'PlatformVersion', width: "50", pinnable: true },

        { field: 'CurrentlyActive', displayName: 'CurrentlyActive', width: "50", pinnable: true },
        
         { field: 'GoogleClientKey', displayName: 'GoogleClientKey', width: "200", pinnable: true },
            { field: 'StateId', displayName: 'StateId', width: "50", pinnable: true },
               { field: 'FleetId', displayName: 'FleetId', width: "50", pinnable: true }
       
        ],
        canSelectRows: false,
        displaySelectionCheckbox: true
    };

    $scope.edit = function (value) {
       
        //check if there are any added messages
        if ($scope.messages == null || $scope.messages.length == 0) {
            window.alert("No Driver Message Message Found. Please Enter a message in the input box");
        }
        else {          
            sendDriverMsg($scope.messages[$scope.messages.length - 1], value.GoogleClientKey, false);
        }


    };

    function fetchData() {
      //  alert("Timeout");
        setTimeout(function () {
            $http({ method: 'GET', url: 'http://localhost:64827/api/currentdriver' }).
  success(function (data, status, headers, config) {
      // this callback will be called asynchronously
      // when the response is available
      //window.alert(data);
      $scope.myData = data;
      if (!$scope.$$phase) {
          $scope.$apply();
      }
  }).
  error(function (data, status, headers, config) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
  });
                  
        }, 1000);       
    }   
    

    function sendDriverMsg(msg, reg, sqlInject) {
     console.log("Request received for:" + reg + " With Message:" + msg);
        customerService.sendMsg(msg, reg, sqlInject)
            .then(function (data) {
                console.log("Done");
                // $scope.customers = data;
                //$scope.gridOptions = { data: 'customers' };
            }, function (error) {
                // error handling here
                console.log(error);
            });
    }

    $scope.messages = [];

    $scope.add = function () {
        $scope.messages.push($scope.drivermessage);
       
        //this is a broadcast message; so send messages to all users
        //for (var i = 0 ; i < $scope.myData.length; i++) {
           
        //     if ($scope.myData[i].GoogleClientKey != "")
        //         sendDriverMsg($scope.drivermessage, $scope.myData[i].GoogleClientKey);
        //}

        $scope.drivermessage = "";

    }

    $scope.broadcast = function () {
        $scope.messages.push($scope.drivermessage);

        //this is a broadcast message; so send messages to all users
        for (var i = 0 ; i < $scope.myData.length; i++) {

            if ($scope.myData[i].GoogleClientKey != "")
                sendDriverMsg($scope.drivermessage, $scope.myData[i].GoogleClientKey, false);
        }

        $scope.drivermessage = "";

    }
    
    fetchData();

});


sampleApp.controller('driversqlcontroller', function ($scope, $http, customerService) {
    //Perform the initialization
    $scope.filterOptions = {
        filterText: ''
    };
    $scope.sqlmessages = [];
    $scope.myData = '';
    $scope.editableInPopup = '<button id="editBtn" type="button" class="btn btn-primary" ng-click="edit(row.entity)" >Inject</button> '
    $scope.gridOptions = {
        data: 'myData',
        showFilter: true,
        showGroupPanel: true,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        columnDefs: [
              { displayName: 'Inject', cellTemplate: $scope.editableInPopup, width: "60", pinnable: true },
        { field: 'DrvId', displayName: 'Id', width: "30", pinnable: true },
        { field: 'DriverNumber', displayName: 'Number', width: "80", pinnable: true },

        { field: 'LastUpdated', displayName: 'LastUpdated', width: "160", pinnable: true },
        { field: 'MobileDeviceId', displayName: 'DeviceId', width: "100", pinnable: true },
        { field: 'MobileEmail', displayName: 'Email', width: "150", pinnable: true },
        
       
        { field: 'MobilePlatform', displayName: 'Type', width: "60", pinnable: true },
        { field: 'MobilePlatformVersion', displayName: 'PlatformVersion', width: "50", pinnable: true },

        
        { field: 'CurrentlyActive', displayName: 'CurrentlyActive', width: "50", pinnable: true },

         { field: 'GoogleClientKey', displayName: 'GoogleClientKey', width: "200", pinnable: true },
            { field: 'StateId', displayName: 'StateId', width: "50", pinnable: true },
               { field: 'FleetId', displayName: 'FleetId', width: "50", pinnable: true }
      
        ],
        canSelectRows: false,
        displaySelectionCheckbox: true
    };

    $scope.edit = function (value) {

        //check if there are any added messages
        if ($scope.sqlmessages == null || $scope.sqlmessages.length == 0) {
            window.alert("No Sql Inject Message Found. Please Enter a Sql command in the input box");
        }
        else {
            //window.alert(value.GoogleClientKey);
            sendDriverSqlMsg($scope.sqlmessages[$scope.sqlmessages.length - 1], value.GoogleClientKey, true);
        }
        //$scope.id = value.id;
        //$scope.name = value.name;
        //$scope.editT = true;

    };

    function fetchData() {
        //  alert("Timeout");
        setTimeout(function () {
            $http({ method: 'GET', url: 'http://localhost:64827/api/currentdriver' }).
  success(function (data, status, headers, config) {
      // this callback will be called asynchronously
      // when the response is available
      //window.alert(data);
      //var index = 0;
      //var simulatedData = [];
      //for (var i = 0; i < 1000; i++) {
       //   for (var j = 0; j < data.length; j++) {
        //      simulatedData.push(data[j]);
         // }
     // }
      $scope.myData = data
     // $scope.myData = simulatedData;
      if (!$scope.$$phase) {
          $scope.$apply();
      }
  }).
  error(function (data, status, headers, config) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
  });

        }, 1000);
    }

    //window.alert("Alert2");

    function sendDriverSqlMsg(msg, reg, sqlInject) {
        console.log("Request received for:" + reg + " With Message:" + msg);
        customerService.sendMsg(msg, reg, sqlInject)
            .then(function (data) {
                console.log("Done");
                // $scope.customers = data;
                //$scope.gridOptions = { data: 'customers' };
            }, function (error) {
                // error handling here
                console.log(error);
            });
    }

   

    $scope.addSql = function () {
        $scope.sqlmessages.push($scope.sqlmessage);

        $scope.sqlmessage = "";

    }


    fetchData();

});

sampleApp.controller('driverdeactivatecontroller', function ($scope, $http, customerService) {
    //Perform the initialization
    $scope.filterOptions = {
        filterText: ''
    };
    //$scope.sqlmessages = [];
    $scope.myData = '';
    $scope.editableInPopup = '<button id="editBtn" type="button" class="btn btn-primary" ng-click="edit(row.entity)" >Deactivate</button> '
    $scope.gridOptions = {
        data: 'myData',
        showFilter: true,
        showGroupPanel: true,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        columnDefs: [
              { displayName: 'Deactivate', cellTemplate: $scope.editableInPopup, width: "100", pinnable: true },
        { field: 'DrvId', displayName: 'Id', width: "30", pinnable: true },
        { field: 'DriverNumber', displayName: 'Number', width: "80", pinnable: true },

        { field: 'LastUpdated', displayName: 'LastUpdated', width: "160", pinnable: true },
        { field: 'MobileDeviceId', displayName: 'DeviceId', width: "100", pinnable: true },
        { field: 'MobileEmail', displayName: 'Email', width: "150", pinnable: true },


        { field: 'MobilePlatform', displayName: 'Type', width: "60", pinnable: true },
        { field: 'MobilePlatformVersion', displayName: 'PlatformVersion', width: "50", pinnable: true },


        { field: 'CurrentlyActive', displayName: 'CurrentlyActive', width: "50", pinnable: true },

         { field: 'GoogleClientKey', displayName: 'GoogleClientKey', width: "200", pinnable: true },
            { field: 'StateId', displayName: 'StateId', width: "50", pinnable: true },
               { field: 'FleetId', displayName: 'FleetId', width: "50", pinnable: true }

        ],
        canSelectRows: false,
        displaySelectionCheckbox: true
    };

    $scope.edit = function (value) {

        //check if there are any added messages
        if (value == null) {
            window.alert("Error in sending Deactivation Message. Please Try Again Later...");
        }
        else {
            sendDriverDeactivation(value.GoogleClientKey);            
        }
        //$scope.id = value.id;
        //$scope.name = value.name;
        //$scope.editT = true;

    };

    function fetchData() {
        //  alert("Timeout");
        setTimeout(function () {
            $http({ method: 'GET', url: 'http://localhost:64827/api/currentdriver' }).
  success(function (data, status, headers, config) {
      // this callback will be called asynchronously
      // when the response is available
      //window.alert(data);
      $scope.myData = data
      //$scope.myData = simulatedData;
      if (!$scope.$$phase) {
          $scope.$apply();
      }
  }).
  error(function (data, status, headers, config) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
  });

        }, 1000);
    }
    function sendDriverDeactivation(reg) {
        console.log("Request received for:" + reg + " With Deactivation request");
        customerService.sendMsg("FRCE_KICK", reg, false)
            .then(function (data) {
                console.log("Done");
                // $scope.customers = data;
                //$scope.gridOptions = { data: 'customers' };
            }, function (error) {
                // error handling here
                console.log(error);
            });
    }


    $scope.addSql = function () {
        $scope.sqlmessages.push($scope.sqlmessage);

        $scope.sqlmessage = "";

    }


    fetchData();

});



sampleApp.controller('driverloggingcontroller', function ($scope, $http, customerService) {
    //Perform the initialization
    $scope.filterOptions = {
        filterText: ''
    };
    //$scope.sqlmessages = [];
    $scope.myData = '';
    $scope.editableInPopup = '<button id="editBtn" type="button" class="btn btn-primary" ng-click="edit(row.entity)" >Pull Logs</button> '
    $scope.gridOptions = {
        data: 'myData',
        showFilter: true,
        showGroupPanel: true,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        columnDefs: [
              { displayName: 'Pull Logs', cellTemplate: $scope.editableInPopup, width: "100", pinnable: true },
        { field: 'DrvId', displayName: 'Id', width: "30", pinnable: true },
        { field: 'DriverNumber', displayName: 'Number', width: "80", pinnable: true },

        { field: 'LastUpdated', displayName: 'LastUpdated', width: "160", pinnable: true },
        { field: 'MobileDeviceId', displayName: 'DeviceId', width: "100", pinnable: true },
        { field: 'MobileEmail', displayName: 'Email', width: "150", pinnable: true },


        { field: 'MobilePlatform', displayName: 'Type', width: "60", pinnable: true },
        { field: 'MobilePlatformVersion', displayName: 'PlatformVersion', width: "50", pinnable: true },


        { field: 'CurrentlyActive', displayName: 'CurrentlyActive', width: "50", pinnable: true },

         { field: 'GoogleClientKey', displayName: 'GoogleClientKey', width: "200", pinnable: true },
            { field: 'StateId', displayName: 'StateId', width: "50", pinnable: true },
               { field: 'FleetId', displayName: 'FleetId', width: "50", pinnable: true }

        ],
        canSelectRows: false,
        displaySelectionCheckbox: true
    };

    $scope.edit = function (value) {

        //check if there are any added messages
        if (value == null) {
            window.alert("Error in sending Log Pull command. Please Try Again Later...");
        }
        else {
            sendDriverLogPull(value.GoogleClientKey);
        }
        //$scope.id = value.id;
        //$scope.name = value.name;
        //$scope.editT = true;

    };

    function fetchData() {
        //  alert("Timeout");
        setTimeout(function () {
            $http({ method: 'GET', url: 'http://localhost:64827/api/currentdriver' }).
  success(function (data, status, headers, config) {
      // this callback will be called asynchronously
      // when the response is available
      //window.alert(data);
      $scope.myData = data
      //$scope.myData = simulatedData;
      if (!$scope.$$phase) {
          $scope.$apply();
      }
  }).
  error(function (data, status, headers, config) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
  });

        }, 1000);
    }
    function sendDriverLogPull(reg) {
        console.log("Request received for:" + reg + " With Log Pull request");
        customerService.sendMsg("LOG_PULL", reg, false)
            .then(function (data) {
                console.log("Done");
                // $scope.customers = data;
                //$scope.gridOptions = { data: 'customers' };
            }, function (error) {
                // error handling here
                console.log(error);
            });
    }


    $scope.addSql = function () {
        $scope.sqlmessages.push($scope.sqlmessage);

        $scope.sqlmessage = "";

    }


    fetchData();

});