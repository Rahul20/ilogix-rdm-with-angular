﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Http;

namespace RDMAngular.Controllers
{
    class Program
    {
        public static string SendGCMNotification(string apiKey, string postData, string postDataContentType = "application/json")
        {
            ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateServerCertificate);

            //
            // MESSAGE CONTENT
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            //
            // CREATE REQUEST
            HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://android.googleapis.com/gcm/send");
            Request.Method = "POST";
            Request.KeepAlive = false;
            Request.ContentType = postDataContentType;
            Request.Headers.Add(string.Format("Authorization: key={0}", apiKey));
            Request.ContentLength = byteArray.Length;

            Stream dataStream = Request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            //
            // SEND MESSAGE
            try
            {
                WebResponse Response = Request.GetResponse();
                HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;
                if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
                {
                    var text = "Unauthorized - need new token";
                }
                else if (!ResponseCode.Equals(HttpStatusCode.OK))
                {
                    var text = "Response from web service isn't OK";
                }

                StreamReader Reader = new StreamReader(Response.GetResponseStream());
                string responseLine = Reader.ReadToEnd();
                Reader.Close();

                return responseLine;
            }
            catch (Exception e)
            {
            }
            return "error";
        }


        public static bool ValidateServerCertificate(
        object sender,
        X509Certificate certificate,
        X509Chain chain,
        SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        public static void Main(CCREntities1 db,String text, String deviceId, String sqlInject)
        {
            //string deviceId = "APA91bFeNLDWny3CRiTGG57tpSeNr-wyfEMuuQhrjCtWYWCayJWeNMguuzkaCfppY8tq7XPUtJslQ9Baemz1sKug4-4eT3r1JDV_j2EpjvPszMHS1v-5cPZE9-3u3OsymHpAwOeI68CWnNSqJstB3RPFuTjr_883Dg";
            //String text = "This feature allows users to remote control a PDA that is online. The only requirement for the PDA is to be switched ON. The driver need NOT be logged in iLogix." +
            //       "To remote control a device: Select the state and PDA type (Motorola, CN50, M3 etc) and locate the required device from the right hand panel. If you cannot see Driver Numbers and/or Serial Numbers please follow the below steps";
            
           // string message = "some test message";
            string tickerText = "example test GCM";
            string contentTitle = "content title GCM";

            string postData = string.Empty;
            if (text.Equals("FRCE_KICK"))
            {
                postData =
                                        "{ \"registration_ids\": [ \"" + deviceId + "\" ], " +
                                        "\"data\": {\"tickerText\":\"" + tickerText + "\", " +
                                        "\"contentTitle\":\"" + contentTitle + "\", " +
                                        "\"message\": \"" + "TXT:" + text + "\"}}";
                //also update the databse
                CurrentDriver c = db.CurrentDrivers.
                    First(i => i.GoogleClientKey == deviceId);
                    c.LastUpdated = DateTime.Now;
                    c.CurrentlyActive = false;
                    db.SaveChanges();

                
            }
            else if (text.Equals("LOG_PULL"))
            {
                postData =
                                        "{ \"registration_ids\": [ \"" + deviceId + "\" ], " +
                                        "\"data\": {\"tickerText\":\"" + tickerText + "\", " +
                                        "\"contentTitle\":\"" + contentTitle + "\", " +
                                        "\"message\": \"" + "TXT:" + text + "\"}}";
            }
            else if (sqlInject.Contains("false"))
            {
                postData =
                            "{ \"registration_ids\": [ \"" + deviceId + "\" ], " +
                            "\"data\": {\"tickerText\":\"" + tickerText + "\", " +
                            "\"contentTitle\":\"" + contentTitle + "\", " +
                            "\"message\": \"" + "TXT:" + text + "\"}}";
            }
            else
            {
                postData =
            "{ \"registration_ids\": [ \"" + deviceId + "\" ], " +
            "\"data\": {\"tickerText\":\"" + tickerText + "\", " +
            "\"contentTitle\":\"" + contentTitle + "\", " +
            "\"message\": \"" + "SQL_INJECT:" + text + "\"}}";

            }
            string response = SendGCMNotification("AIzaSyDqtxCCZwaJbPiSsmdwaCqCMqrKLWJApZ0",  postData);
        }
    }
    public class CurrentDriverController : ApiController
    {
        private CCREntities1 db = new CCREntities1();

        // GET api/CurrentDriver
        public IEnumerable<CurrentDriver> GetCurrentDrivers()
        {
            return db.CurrentDrivers.AsEnumerable();
        }

        
        // GET api/CurrentDriver/5
        public CurrentDriver GetCurrentDriver(int id, string text, string regId, string sqlInject)
        {
            CurrentDriver currentdriver = null;// db.CurrentDrivers.Find(id);
            
            int bytes = System.Text.ASCIIEncoding.Unicode.GetByteCount(text);
            //restrict GCM messages to be 4 Kb
            if (bytes/1024 <=4)
                Program.Main(db,text,regId, sqlInject);
            return currentdriver;
        }

        // PUT api/CurrentDriver/5
        public HttpResponseMessage PutCurrentDriver(int id, CurrentDriver currentdriver)
        {
         //   Program.Main();
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != currentdriver.DrvId)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(currentdriver).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/CurrentDriver
        public HttpResponseMessage PostCurrentDriver(CurrentDriver currentdriver)
        {
            if (ModelState.IsValid)
            {
                db.CurrentDrivers.Add(currentdriver);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, currentdriver);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = currentdriver.DrvId }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/CurrentDriver/5
        public HttpResponseMessage DeleteCurrentDriver(int id)
        {
            CurrentDriver currentdriver = db.CurrentDrivers.Find(id);
            if (currentdriver == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.CurrentDrivers.Remove(currentdriver);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, currentdriver);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}